package de.awacademy.user_stories.comment;

import javax.validation.constraints.Size;

public class CommentDTO {

    @Size(min = 1, max = 90)
    private String text;

    public CommentDTO(@Size(min = 1, max = 90) String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
