package de.awacademy.user_stories.comment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {
    List<Comment> findAllByOrderByPostedAtAsc();
    List<Comment> findAll();
}
