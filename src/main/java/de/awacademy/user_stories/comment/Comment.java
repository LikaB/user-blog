package de.awacademy.user_stories.comment;

import de.awacademy.user_stories.blogPost.Post;
import de.awacademy.user_stories.user.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class Comment {

    @Id
    @GeneratedValue
    private long id;

    private String text;
    private Instant postedAt;

    @ManyToOne
    private Post post;

    @ManyToOne
    private User user;


    public Comment() {
    }

    public Comment(String text, Instant postedAt, Post post, User user) {
        this.text = text;
        this.postedAt = postedAt;
        this.post = post;
        this.user = user;
    }

    public Comment(String text, Instant postedAt, Post post) {
        this.text = text;
        this.postedAt = postedAt;
        this.post = post;
    }


    public Comment(String text, Instant postedAt) {
        this.text = text;
        this.postedAt = postedAt;
    }



    public Comment(String text, User user) {
        this.text = text;
        this.user = user;
    }


    public String getText() {
        return text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
