package de.awacademy.user_stories.blogPost;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {
     private PostRepository postRepository;

     @Autowired
     public PostService(PostRepository postRepository){
         this.postRepository= postRepository;
     }

      public List<Post>getPost(){
         return postRepository.findAllByOrderByPostedAtDesc();

      }

      public void savePost(Post post){
         postRepository.save(post);
      }

    }

