package de.awacademy.user_stories.blogPost;

import javax.validation.constraints.Size;

public class PostDTO {

    @Size(min = 1, max = 10000)
    private String text;
    @Size(min=1,max=500)
    private String titel;


    public PostDTO(@Size(min = 1, max = 1000) String text, String titel)
    {
        this.text = text;
        this.titel=titel;
    }


    public String getText()
    {
        return text;
    }

    public String getTitel() {
        return titel;
    }
}
