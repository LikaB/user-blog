package de.awacademy.user_stories.blogPost;

import de.awacademy.user_stories.comment.Comment;
import de.awacademy.user_stories.user.User;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;


@Entity
public class Post {

    @Id
    @GeneratedValue
    private long id;

    @Lob
    private String text;
    private String titel;
    private Instant postedAt;

    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "post")
    private List<Comment> comments;


    public Post() {
    }


    public Post(User user, String text, Instant postedAt, String titel) {
        this.text = text;
        this.user = user;
        this.postedAt = postedAt;
        this.titel=titel;
    }



    public User getUser() {
        return user;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText()
    {
        return text;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitel() {
        return titel;
    }



}
