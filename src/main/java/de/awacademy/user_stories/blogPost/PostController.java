package de.awacademy.user_stories.blogPost;

import de.awacademy.user_stories.comment.Comment;
import de.awacademy.user_stories.comment.CommentDTO;
import de.awacademy.user_stories.comment.CommentRepository;
import de.awacademy.user_stories.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.time.Instant;


@Controller
public class PostController {

    private PostService postService;
    private PostRepository postRepository;
    private CommentRepository commentRepository;


    @Autowired
    public PostController(PostService postService,
                          PostRepository postRepository,
                          CommentRepository commentRepository){
        this.postService = postService;
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
    }

    @GetMapping("/post")
    public String post(Model model) {
        model.addAttribute("post", new PostDTO("",""));
        return "post";
    }



    @PostMapping("/post")
    public String post(@Valid @ModelAttribute("post") PostDTO postDTO,
                       BindingResult bindingResult, @ModelAttribute("sessionUser") User sessionUser)
                        {
        if (bindingResult.hasErrors()) {
            return "post";
        }
                            Post post = new Post(sessionUser, postDTO.getText(),Instant.now(),postDTO.getTitel());
            postRepository.save(post);

                            return "redirect:/";
    }



    @GetMapping("/postview/{id}")
    public String postView(@PathVariable long id, Model model) {
        Post post = postRepository.findById(id);
        model.addAttribute("post", post);
        model.addAttribute("comment", new Comment());
        model.addAttribute("comment", new CommentDTO(""));

        return "postview";
    }



    @PostMapping("/postview/{id}")
    public String comment(@PathVariable long id, @Valid @ModelAttribute("comment") CommentDTO commentDTO, BindingResult bindingResult,
                          @ModelAttribute("sessionUser") User sessionUser) {
        if (bindingResult.hasErrors()) {
            return "postview";
        }

        Comment comment = new Comment(commentDTO.getText(), Instant.now(), postRepository.findById(id), sessionUser);
        commentRepository.save(comment);

        return "redirect:/postview/" + id;
    }

}
