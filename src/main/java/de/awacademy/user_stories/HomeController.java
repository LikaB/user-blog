package de.awacademy.user_stories;

import de.awacademy.user_stories.blogPost.PostService;
import de.awacademy.user_stories.comment.CommentRepository;
import de.awacademy.user_stories.user.User;
import de.awacademy.user_stories.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HomeController {

    private PostService postService;
    private CommentRepository commentRepository;
    private UserRepository userRepository;


    @Autowired
    public HomeController(PostService postService, CommentRepository commentRepository, UserRepository userRepository)

    {
        this.postService = postService;
        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/")
    public String home(@ModelAttribute("sessionUser") User sessionUser, @ModelAttribute("user") User user, Model model) {
        model.addAttribute("posts", postService.getPost());
        model.addAttribute("comments", commentRepository.findAllByOrderByPostedAtAsc());

        return "home";
    }

}
