package de.awacademy.user_stories.user;


import de.awacademy.user_stories.blogPost.Post;
import de.awacademy.user_stories.comment.Comment;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue
    private long id;

    private String username;
    private String password;
    private boolean hasAdminRights;

    @OneToMany(mappedBy = "user")
    private List<Post> posts;

    @OneToMany(mappedBy = "user")
    private List<Comment> comments;

    public User (){

    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, boolean hasAdminRights) {
        this.username = username;
        this.hasAdminRights = hasAdminRights;
    }

    public User(String username, String password, boolean hasAdminRights) {
        this.username = username;
        this.password = password;
        this.hasAdminRights = hasAdminRights;
    }

    public List<Post> getPosts()
    {
        return posts;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    public List<Comment> getComments() {
        return comments;
    }


    public boolean isHasAdminRights() {
        return hasAdminRights;
    }

}
